

This project is a C language path finding algorythm.

It find the fastest way to transfer N units from start point to end point on a
network.

Given that a node cannot contain more than one unit per turn, and that all nodes
are equidistant, the algorithm strives to find the best combination of disjoint
paths.

There is a simple map generator and plenty of pre generated maps in the maps folder.

The launch line command is:

./lem-in < map
